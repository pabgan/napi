#!/bin/zsh

##########################
# 0. Auxiliary functions
#

##########################
# 1. PARSE INPUT
zparseopts -F -D -verbose=o_verbose -edit=o_edit 
edit="${o_edit[1]}"

help_message="Syntax is: $(basename $0) [--verbose] [--edit]"

#if [[ $# > 1 ]]; then
#	echo "ERROR: invalid syntax." >/dev/stderr
#	echo "$help_message" >/dev/stderr
#	exit 2
#fi

if [[ $o_verbose ]]; then echo ">INFO> Verbose mode" >/dev/stderr ; fi

templates_path="${0%/*}/soap_request_templates/"
if [[ $o_verbose ]]; then echo ">INFO> Looking for templates in $templates_path" >&2 ; fi
request_type=$( find $templates_path -maxdepth 3 -mindepth 2 -type d | fzf --header "Please, chose a request" --delimiter / --with-nth 8..)

pushd $request_type
if [[ $o_verbose ]]; then echo ">INFO> Looking for templates in $templates_path/$request_type" >&2 ; fi
request=$(cat $(find_upwards.sh --all request-top.xml | tac))
if [[ $o_verbose ]]; then echo ">DEBUG> request='$request'" >&2 ; fi

request="$request\n$(cat (find options -type f 2> /dev/null | fzf --multi ))"
if [[ $o_verbose ]]; then echo ">DEBUG> request='$request'" >&2 ; fi

request="$request\n$(cat $(find_upwards.sh --all request-bottom.xml | tac))"
if [[ $o_verbose ]]; then echo ">DEBUG> request='$request'" >&2 ; fi
popd

if [[ $edit ]]; then
	if [[ $o_verbose ]]; then echo "editing..." >/dev/stderr ; fi

	tmp_file='/tmp/napi_request_builder.xml'
	echo $request > $tmp_file
	$EDITOR $tmp_file
	cat $tmp_file
else
	echo $request
fi
