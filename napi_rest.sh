#!/bin/zsh

##########################
# Auxiliary functions
#
remove_token_if_old() {
	## 1.1 Remove cached credentials if they are too old
	# Variable below expressed in minutes
	max_old=10
	find .token -mmin +$max_old -exec rm {} \; 2> /dev/null
}

get_token() {
	get_token_command="curl --silent \
		                --user \"rest-client-trusted:gnQB_jC-XU8RB*3#\" \
				--insecure \
				--request POST \
				--data \"username=$USER&password=$PASSWORD&grant_type=password\" \
				$ADDRESS/$ENDPOINT/oauth/token"
	if [[ $verbose ]]; then echo "> acquiring token with:\n$get_token_command" ; fi
	eval "${get_token_command}" | cut -d'"' -f4 > .token
}

complete_request_parameters() {
	# The first and only parameter is the URI
	case "$1" in
		## REALTIME
		(realtime/v1/reports) ACTION='GET' ;;
		(realtime/v1/reports/DATA_COLLECTION/search*) ACTION='GET' ;;
		(realtime/v1/reports/single/REALTIME_PO/*) ACTION='POST' ;;
		(realtime/v1/reports/single/PON_DATA_COLLECTION/*) ACTION='POST' ;;
		(realtime/v1/reports/bulk/REALTIME_PO/DSLAM/*) ACTION='POST' ;;
		(realtime/v1/reports/PON_DATA_COLLECTION/fields) ACTION='GET' ;;
		(realtime/v1/reports/REALTIME_PO/dates/*) ACTION='GET' ;;
		(realtime/v1/reports/*) ACTION='GET' ;;
		(realtime/v1/reports/PON_DATA_COLLECTION/running/*) ACTION='GET' ;;
		(realtime/v1/reports/PON_DATA_COLLECTION/search/filters) ACTION='GET' ;;
		(realtime/v1/reports/PON_DATA_COLLECTION/allowed/*) ACTION='GET' ;;
		
		## PEDATA
		(pedata/v1/reports) ACTION='GET' ;;
		(pedata/v1/reports/PON_OPERATIONAL_DATA/fields) ACTION='GET' ;;
		(pedata/v1/reports/PON_OPERATIONAL_DATA/dates/LINE_ID/*) ACTION='GET' ;;
		(pedata/v1/reports/ONT_DIAGNOSTICS_DATA/data/LINE_ID/*) ACTION='GET' ;;
		(pedata/v1/reports/LINE_SUMMARY_DATA/messages/es) ACTION='GET' ;;
		(pedata/v1/reports/SERVICE_RECOMMENDATION_DATA_BY_TBOX/fields) ACTION='GET' ;;
		(pedata/v1/reports/SERVICE_RECOMMENDATION_DATA_BY_TBOX/data/*) ACTION='GET' ;;
		(pedata/v1/reports/GHN_OPERATIONAL_DATA/fields) ACTION='GET' ;;

		## PROFILEOPTIMIZATION
		(profileoptimization/v1/fields) ACTION='GET' ;;
		(profileoptimization/v1/lines/*/request) ACTION='POST' ;;
		(profileoptimization/v1/lines/*/abort) ACTION='POST' ;;
		(profileoptimization/v1/lines/*/status) ACTION='GET' ;;
		(profileoptimization/v1/lines/*/record) ACTION='GET' ;;
		(profileoptimization/v1/lines/*/dates) ACTION='GET' ;;
		(profileoptimization/v1/lines/*/enable) ACTION='GET' ;;
		(profileoptimization/v1/lines/*/disable) ACTION='GET' ;;

		## PROVISIONING
		(provisioning/v1/dslamtypes) ACTION='GET' ;;
		(provisioning/v1/serviceproducts) ACTION='GET' ;;
		(provisioning/v1/technologies) ACTION='GET' ;;
		(provisioning/v1/lines/*)
			if [ -z ${ADDITIONAL_PARAMETERS} ]; then
				ACTION='GET';
			else
				ACTION='POST';
			fi
			;;
		(provisioning/v1/lines) ACTION='POST' ;;
		(provisioning/v1/lines/*) ACTION='DELETE' ;;
		(provisioning/v1/lines/*/reactivate) ACTION='POST' ;;
		(provisioning/v1/dslams/HUAWEI1/lines*) ACTION='POST' ;;
		(provisioning/v1/lines/search*) ACTION='GET' ;;
		(provisioning/v1/lines/search/filters) ACTION='GET' ;;
		(provisioning/v1/lines/*/history*) ACTION='GET' ;;
		(provisioning/v1/lines/bonded/*) ACTION='GET' ;;
		(provisioning/v1/dslams/*)
			if [ -z ${ADDITIONAL_PARAMETERS} ]; then
				ACTION='GET';
			else
				ACTION='POST';
			fi
			;;
		(provisioning/v1/dslams) ACTION='POST' ;;
		(provisioning/v1/dslams/*) ACTION='DELETE' ;;
		(provisioning/v1/dslams/*/reactivate) ACTION='POST' ;;
		(provisioning/v1/dslams/search/filters) ACTION='GET' ;;
		(provisioning/v1/dslams/search*) ACTION='GET' ;;
		(provisioning/v1/dslams/*/history*) ACTION='GET' ;;
		(provisioning/v1/neighborhood/*) ACTION='GET' ;;
		(provisioning/v1/neighborhood/search/filters) ACTION='GET' ;;
		(provisioning/v1/neighborhood/fields) ACTION='GET' ;;
		(provisioning/v1/neighborhood) ACTION='POST' ;;
		(provisioning/v1/neighborhood/*) ACTION='POST' ;;
		(provisioning/v1/neighborhood/*) ACTION='DELETE' ;;
		(provisioning/v1/cable/search/filters) ACTION='GET' ;;
		(provisioning/v1/cable/search*) ACTION='GET' ;;
		(provisioning/v1/lines/*/config-groups) ACTION='GET' ;;
		(provisioning/v1/lines/config-groups) ACTION='GET' ;;
		(provisioning/v1/lines/*/config-groups/history*) ACTION='GET' ;;
		## COMMANDE
		(commande/v1/recommendations/LINE_ID/*) ACTION='GET' ;;
		(*) echo ">> WARN: URI not recognized. Will be sent with no further management." >/dev/stderr ;;
	esac
}

##########################
# PARSE INPUT
zparseopts -F -D -verbose=verbose -env:=env -endpoint:=endpoint -port:=port -user:=user -password:=password -additional-parameters:=additional_parameters -options:=opts -clear-token=clear_token -dry-run=dry_run -action=action
verbose="${verbose[1]}"
clear_token="${clear_token[1]}"
dry_run="${dry_run[1]}"
env="${env[2]:-$CUSTOMER_ENVIRONMENT}"
ACTION="${action[2]:-GET}"
ENDPOINT="${endpoint[2]:-expresse}"
PORT="${port[2]:-8443}"
USER="${user[2]:-administrator}"
PASSWORD="${password[2]:-assia}"
PROTOCOL="${protocol[2]:-https}"
ADDITIONAL_PARAMETERS="${additional_parameters[2]}"
OPTS="${opts[2]}"
ADDRESS="${PROTOCOL}://${env}:${PORT}"

request='curl --silent --insecure --request $ACTION --url $ADDRESS/$ENDPOINT/rest/$URI --header "accept: application/json" --header "Authorization: bearer $token" --header "cache-control: no-cache" --header "content-type: application/json" ${ADDITIONAL_PARAMETERS:+--data} ${ADDITIONAL_PARAMETERS} ${OPTS}'

help_message="Syntax is: $(basename $0) [--verbose] [--env env.ironm.ent] [options] URI

	More info can be found in: https://gitlab.assia-inc.com/expresse/expresse-web/expresse_rest_client/-/blob/master/src/site/markdown/curl.commands.md"

if [[ $# < 1 ]]; then
	echo "ERROR: invalid syntax." >/dev/stderr
	echo "$help_message" >/dev/stderr
	exit 2
fi

if [[ $verbose ]]; then echo "> Verbose mode" >/dev/stderr ; fi

URI=${1}
shift

##########################
# MAIN
# 
# 1. Get authorization if needed
if [[ $clear_token ]]; then rm -f .token; fi
remove_token_if_old
if [[ ! -e .token ]]; then
	get_token
else
	if [[ $verbose ]]; then echo "> reusing token" ; fi
	touch .token
fi
token=$(cat .token)

## Exit with error if token could not be procured
if [[ -z $token || $token == 'invalid_grant' ]]; then
	echo ">> CRITICAL: could not get valid token. Can not proceed further" >/dev/stderr
	exit 1
fi

# 2. Complete known request parameters based on endpoint
complete_request_parameters $URI

if [[ $verbose ]]; then echo "> executing request:"; eval "echo $request" ; fi
if [[ -z $dry_run ]]; then
	eval "$request"
else
	echo ">> but --dry-run is activated"
fi
