#!/bin/zsh
##########################
# CONFIGURATION
#
sessionid_path=/tmp/sessionid
EXEC_PATH=$(pwd)
USER_ID='administrator'
USER_PASSWORD='assia'
ENDPOINT='expresse'
CONTENT_TYPE_11='Content-Type: text/xml;charset=UTF-8'
CONTENT_TYPE_12='Content-Type: application/soap+xml;charset=UTF-8;action="${(e)action}"'
CURL_COMMAND='curl --silent \
                   ${verbose:+-vvv} \
                   --http1.1 \
		   --header "User-Agent: Apache-HttpClient/4.5.5 (Java/16.0.1)" \
		   --header "${(e)content_type}" \
                   --header "Accept-Encoding: deflate" \
		   ${sessionid:+--header} ${sessionid:+cookie: JSESSIONID=$sessionid} \
		   ${soap_action:+--header} ${(e)soap_action} \
                   --data ''${(e)envelope}'' \
                   ${protocol}://${env}:${port}/${ENDPOINT}/services/${service}${protocol/http/Http}Soap${soap_version}Endpoint/'


##########################
# AUXILIARY FUNCTIONS
#

_get_sessionid(){
	if [[ $verbose ]]; then echo ">DEBUG> renewing $sessionid_path..." >&2 ; fi

	envelope='<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:impl="http://impl.api.authentication.dslo.assia.com">
   <soap:Header/>
   <soap:Body>
      <impl:login>
         <impl:userId>${user_id:-$USER_ID}</impl:userId>
         <impl:password>${user_password:-$USER_PASSWORD}</impl:password>
      </impl:login>
   </soap:Body>
</soap:Envelope>'

	_infere_stuff
	_execute_command | sed -E 's/.*sessionId>([^<]+)<.*/\1\n/' > $sessionid_path
}

_execute_command() {
	if [[ $verbose ]]; then echo ">DEBUG> About to execute: ${(e)CURL_COMMAND}" >&2 ; fi

	if [[ -z $dry_run ]]; then
		eval ${CURL_COMMAND}
	else
		echo ">WARN> Not sending any request because --dry-run is active" >&2
	fi
}

_infere_stuff() {
	# - Soap Version
	namespace=$(grep -o -E "xmlns:soap.*=\".*\"" <(echo ${envelope}) | cut -d'"' -f2)
	case $namespace in
		('http://www.w3.org/2003/05/soap-envelope')
			soap_version='12' ;
			content_type=$CONTENT_TYPE_12;
			;;
		('http://schemas.xmlsoap.org/soap/envelope/')
			soap_version='11' ;
			content_type=$CONTENT_TYPE_11 ;
			soap_action='SOAPAction: "${action}"'
			;;
		*) echo ">ERROR> Namespace '$namespace' not recognized" >&2 ; exit 2;;
	esac

	# - action
	action=$(grep '<soap.*:Body>' -A1 <(echo ${envelope}) | tail -n1 | sed -E 's/<([^:]+:[^>]+)>/\1/' | tr -d '\t ')
	action_key=$(echo $action | sed -E 's/^<//' | sed -E 's/:.*//')
#	if [[ $soap_version == '12' ]]; then
		action=${action/impl/urn}
#	fi

	# - service
	service_url=$(grep '<soap.*:Envelope' <(echo ${envelope}) | grep -o -E "xmlns:$action_key=\".*\"" | cut -d'"' -f2)
	case $service_url in
		'http://impl.api.authentication.dslo.assia.com')
			service="authentication.authentication"
			;;
		'http://impl.customization.dslo.assia.com')
			service="customization.customization"
			;;
		'http://impl.identity.dslo.assia.com')
			service="identity.identity"
			;;
		'http://v2.impl.identity.dslo.assia.com')
			service="identity_v2.identity_v2"
			;;
		'http://report.neighborhood.dslo.assia.com')
			service="neighborhood.neighborhood"
			;;
		'http://network.module.statistics.dslo.assia.com')
			service="network_statistics.network_statistics"
			;;
		'http://osp.module.statistics.dslo.assia.com')
			service="osp_statistics.osp_statistics"
			;;
		'http://ws.api.report.dslo.assia.com')
			service="pe_data.pe_data"
			;;
		'http://estimator.pe.module.statistics.dslo.assia.com')
			service="pe_statistics.pe_statistics"
			;;
		'http://impl.po.dslo.assia.com')
			service="profile_optimization.profile_optimization"
			;;
		'http://impl.provisioning.dslo.assia.com')
			service="provisioning_info.provisioning_info"
			;;
		'http://impl.realtime.dslo.assia.com')
			service="realtime.realtime"
			;;
		'http://impl.reporting.dslo.assia.com')
			service="custom_report.custom_report"
			;;
		*)
			echo -e ">WARN> $service_url not implemented yet.\naction='$action', action_key='$action_key'" >&2
			#exit 2
			;;
	esac

	if [[ $verbose ]]; then echo ">DEBUG> Parsed: action='$action', action_key='$action_key', service_url='$service_url', namespace='$namespace'" >&2 ; fi
}


##########################
# MAIN
#
#####################
#
## 0. Parse input
# TODO: add options to override configuration defaults
zparseopts -F -D -verbose=o_verbose -dry-run=dry_run -env:=o_env -credentials:=o_credentials -clean-session=clean_session -https=https -port:=o_port
verbose="${o_verbose[1]}"
dry_run="${dry_run[1]}"
env="${o_env[2]:-$CUSTOMER_ENVIRONMENT}"
user_id="${o_credentials[2]%%:*}"
user_password="${o_credentials[2]#*:}"
clean_session="${clean_session[1]}"
port=${o_port[2]:-8080}
protocol=${${https[1]/--/}:-http}

#if [[ $verbose ]]; then echo ">DEBUG> protocol='$protocol'" >&2; fi

help_message="Syntax is: $(basename $0) [--verbose] [--dry-run] [--env env.ironm.ent] [--credentials username:password] [--clean-session] [--https] [--port port] request.xml"

if [[ $# < 1 ]]; then
	echo ">ERROR> invalid syntax." >&2
	echo "$help_message" >&2
	exit 2
fi

envelope_name=${1}; shift

if [[ $verbose ]]; then echo ">DEBUG> Verbose mode" >&2 >&2 ; fi

## 1. Get a session
#
if [[ $clean_session ]]; then
	rm $sessionid_path
else
	# Remove cached credentials if they are too old
	# in minutes
	MAX_OLD=10
	find $sessionid_path -mmin +$MAX_OLD -exec rm {} \; 2> /dev/null
	sessionid=$(cat $sessionid_path 2> /dev/null )
fi

if [[ -z $sessionid ]]; then
	 _get_sessionid
	sessionid=$(cat $sessionid_path)
else
	if [[ $verbose ]]; then echo ">DEBUG> reusing $sessionid_path" >&2 ; fi
fi

if [[ $verbose ]]; then echo ">DEBUG> sessionid=$sessionid" >&2 ; fi

#####################
## 2. Create command
envelope=$(cat $envelope_name)
_infere_stuff

#####################
## 3. Send request
output=$(_execute_command)

# and print it
echo $output
