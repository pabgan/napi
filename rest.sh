#!/bin/zsh

##########################
# Config
#
requests_path=${XDG_CONFIG_HOME}/napi
request_template='curl --silent --insecure --request $ACTION --url $ADDRESS/$ENDPOINT/$URI --header "accept: application/json" --header "Authorization: bearer $token" --header "cache-control: no-cache" --header "content-type: application/json" ${ADDITIONAL_PARAMETERS:+--data} "${ADDITIONAL_PARAMETERS}" ${OPTS}'
token_path=/tmp/token

##########################
# Auxiliary functions
#
remove_token_if_old() {
	## 1.1 Remove cached credentials if they are too old
	# Variable below expressed in minutes
	max_old=10
	find $token_path -mmin +$max_old -exec rm {} \; 2> /dev/null
}

get_token() {
	get_token_command="curl --silent \
		                --user \"rest-client-trusted:gnQB_jC-XU8RB*3#\" \
				--insecure \
				--request POST \
				--data \"username=$USER&password=$PASSWORD&grant_type=password\" \
				$ADDRESS/$ENDPOINT/oauth/token"
	if [[ $verbose ]]; then echo "> acquiring token with:\n$get_token_command" >&2; fi
	eval "${get_token_command}" | cut -d'"' -f4 > $token_path
}

build_request() {
	if [[ $verbose ]]; then echo ">DEBUG> Building request interactively" >&2 ; fi

	################################
	# 1. First user choses a request
	#
	request_chosen=$(find $requests_path/rest/ -type f -not -name '.*' | fzf --header "Please, chose a request" --delimiter / --with-nth 7..)
	request_chosen=${request_chosen#$requests_path}
	if [[ $verbose ]]; then echo ">DEBUG> requests_path=$requests_path && request_chosen=$request_chosen" >&2 ; fi

	################################
	# 2. Then we complete the curl parameters
	#
	# The file name should be the name of the REST ACTION: GET, POST, DELETE
	ACTION=${request_chosen##*/}
	if [[ $verbose ]]; then echo ">DEBUG> ACTION=$ACTION" >&2 ; fi

	# The endpoint (URI) is everything but the ACTION
	URI=${request_chosen%/*}
	URI=${URI#/}
	if [[ $verbose ]]; then echo ">DEBUG> URI=$URI" >&2 ; fi

	################################
	# 3. Then we complete the missing keys
	#
	keys_to_populate=$(echo $request_chosen | grep -oE '\{[^}]+\}')
	keys=(${(@f)keys_to_populate})
	if [[ $verbose ]]; then echo ">DEBUG> keys=$keys" >&2 ; fi

	# Create an associative array
	typeset -A values
	for key in $keys; do
		# We ask the user to chose among the values provided from a script
		# in the key directory
		key_values_path=$requests_path/${request_chosen%${key}*}/.key_values.sh
		if [[ -e $key_values_path ]]; then
			if [[ $verbose ]]; then echo ">DEBUG> Searching $key values in: $key_values_path" >&2 ; fi
			selected_line=$(sh $key_values_path $values | fzf --header "Please, choose a value for $key" --no-preview)
			selected_line=(${(z)selected_line})
			value=$selected_line[1]
			values+=([$key]=$value)
			if [[ $verbose ]]; then echo ">DEBUG> Selected $key=$value" >&2 ; fi
		else
			echo ">ERROR> $key_values_path not found." >&2
			exit
		fi

		# And we directly substitute it in the URI
		URI=${URI/$key/$value}
		if [[ $verbose ]]; then echo ">DEBUG> Updated URI=$URI" >&2 ; fi
	done

	if [[ ${ADDITIONAL_PARAMETERS} == 'template' ]]; then
		# We have to remove the GET/POST or whatever from $request_chosen
		ap_template_path="${requests_path}/${request_chosen%/*}/.additional_parameters"
		if [[ $verbose ]]; then echo ">DEBUG> Using additional parameters template from '$ap_template_path'" >&2 ; fi
		cp $ap_template_path .
		$EDITOR .additional_parameters
		ADDITIONAL_PARAMETERS='.additional_parameters'
	fi
}


##########################
# PARSE INPUT
zparseopts -F -D -verbose=verbose -protocol:=protocol -env:=env -endpoint:=endpoint -port:=port  -action:=action -user:=user -password:=password -additional-parameters:=additional_parameters -options:=opts -clear-token=clear_token -dry-run=dry_run
verbose="${verbose[1]}"
clear_token="${clear_token[1]}"
dry_run="${dry_run[1]}"
env="${env[2]:-$CUSTOMER_ENVIRONMENT}"
ACTION="${action[2]:-GET}"
ENDPOINT="${endpoint[2]:-expresse}"
PORT="${port[2]:-8443}"
USER="${user[2]:-administrator}"
PASSWORD="${password[2]:-assia}"
PROTOCOL="${protocol[2]:-https}"
ADDITIONAL_PARAMETERS="${additional_parameters[2]}"
OPTS="${opts[2]}"
ADDRESS="${PROTOCOL}://${env}:${PORT}"

help_message="Syntax is: $(basename $0) [--verbose] [--env env.ironm.ent] [options] URI

	More info can be found in: https://gitlab.assia-inc.com/expresse/expresse-web/expresse_rest_client/-/blob/master/src/site/markdown/curl.commands.md"

if [[ $# > 2 ]]; then
	echo "ERROR: invalid syntax." >&2
	echo "$help_message" >&2
	exit 2
fi

if [[ $verbose ]]; then echo ">INFO> Verbose mode" >&2 ; fi

URI=${1}

##########################
# MAIN
# 
# 1. Get authorization if needed
if [[ $clear_token ]]; then rm -f $token_path; fi
remove_token_if_old
if [[ ! -e $token_path ]]; then
	get_token
else
	if [[ $verbose ]]; then echo ">INFO> reusing token" >&2; fi
	touch $token_path
fi
token=$(cat $token_path)

## Exit with error if token could not be procured
if [[ -z $token || $token == 'invalid_grant' ]]; then
	echo ">CRITICAL> Could not get valid token. Can not proceed further" >&2
	exit 1
fi

# 2. If no URI was provided, build one interactively
if [[ $URI == "" ]]; then
	build_request
fi

# 3. Then we complete the additional params if needed
#
if [[ -n ${ADDITIONAL_PARAMETERS} ]]; then
	if [[ $verbose ]]; then echo ">DEBUG> Reading additional parameters contents from ${ADDITIONAL_PARAMETERS}" >&2 ; fi
	ADDITIONAL_PARAMETERS=$(cat ${ADDITIONAL_PARAMETERS})
fi

# 4. Finnally we execute the request
if [[ $verbose ]]; then echo ">DEBUG> executing request: ${(e)request_template}" >&2; fi
if [[ -z $dry_run ]]; then
	echo ">INFO> Sending $URI to $env" >&2
	eval "$request_template"
else
	echo ">WARN> Not sending any request because --dry-run is active" >&2
fi
